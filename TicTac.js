console.log("TicTac");
let play = 'p1';
let ids = [];
let values = [];

for (i = 0; i < 9; i++) {
    ids.push(i);
}

getUpdatedValues();

function switchPlayer(currentPlayer) {
    play = currentPlayer == 'p1' ? 'p2' : 'p1';
}

function handleClick(id) {
    getUpdatedValues();
    if (document.getElementById(id).innerHTML == '') {
        if (play == 'p1') {
            var a = document.getElementById(id).innerHTML = 'XX';
            console.log(":::a", a)
        } else if (play == 'p2') {
            var b = document.getElementById(id).innerHTML = 'OO';
            console.log("::b", b)
        }
        switchPlayer(play);
        getUpdatedValues();
    }
    jeetOrHaar(values);
}

function reset() {
    ids.forEach((val) => {
        document.getElementById(val).innerHTML = "";
    });
}

function jeetOrHaar(valArray) {
    if ((valArray[0] == "XX" && valArray[1] == "XX" && valArray[2] == "XX") ||
        (valArray[3] == "XX" && valArray[4] == "XX" && valArray[5] == "XX") ||
        (valArray[6] == "XX" && valArray[7] == "XX" && valArray[8] == "XX") ||
        (valArray[0] == "XX" && valArray[3] == "XX" && valArray[6] == "XX") ||
        (valArray[0] == "XX" && valArray[4] == "XX" && valArray[8] == "XX") ||
        (valArray[2] == "XX" && valArray[4] == "XX" && valArray[6] == "XX") ||
        (valArray[1] == "XX" && valArray[4] == "XX" && valArray[7] == "XX") ||
        (valArray[2] == "XX" && valArray[5] == "XX" && valArray[8] == "XX")) {
        alert("player (XX) jeeta");
        reset();
    }
    if ((valArray[0] == "OO" && valArray[1] == "OO" && valArray[2] == "OO") ||
        (valArray[3] == "OO" && valArray[4] == "OO" && valArray[5] == "OO") ||
        (valArray[6] == "OO" && valArray[7] == "OO" && valArray[8] == "OO") ||
        (valArray[0] == "OO" && valArray[3] == "OO" && valArray[6] == "OO") ||
        (valArray[0] == "OO" && valArray[4] == "OO" && valArray[8] == "OO") ||
        (valArray[2] == "OO" && valArray[4] == "OO" && valArray[6] == "OO") ||
        (valArray[1] == "OO" && valArray[4] == "OO" && valArray[7] == "OO") ||
        (valArray[2] == "OO" && valArray[5] == "OO" && valArray[8] == "OO")) {
        alert("player (OO) jeeta");
        reset();
    }

}

function getUpdatedValues() {
    values = [];
    ids.forEach((val) => {
        let v = document.getElementById(val).innerHTML;
        if (!v || v == '') {
            values.push(null);
        } else {
            values.push(v);
        }
    })
    console.log(":::v", values);
}